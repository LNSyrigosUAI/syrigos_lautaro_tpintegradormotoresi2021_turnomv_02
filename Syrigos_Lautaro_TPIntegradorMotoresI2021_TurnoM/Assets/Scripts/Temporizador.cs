﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class Temporizador : MonoBehaviour
{
    public Text txtTemporizador;
    private float Inicio;
    private bool Terminaste = false;
    // Start is called before the first frame update
    void Start()
    {
        Inicio = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (Terminaste)
            return;
        float t = Time.time - Inicio;
        string minutos = ((int)t / 60).ToString();
        string segundos = (t % 60).ToString("f2");
        txtTemporizador.text = minutos + ":" + segundos;
    }
    public void Terminar()
    {
        Terminaste = true;
        txtTemporizador.color = Color.cyan;
    }
}
