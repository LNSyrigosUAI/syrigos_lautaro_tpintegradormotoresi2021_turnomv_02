﻿using System.Collections;
using UnityEngine;

public class Cierre : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        GameObject.Find("Jugador").SendMessage("Terminar");
    }
}
