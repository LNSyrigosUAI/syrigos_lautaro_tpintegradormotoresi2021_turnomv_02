﻿using UnityEngine;
using UnityEngine.UI;
public class ControlJugador : MonoBehaviour
{
    //Para hacer mecanica de salto.
    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;
    private Rigidbody rb;
    private int cont;
    //UI.
    public Text textoCantidadRecolectados;
    public Text textoGanaste;
    //---------
    public float rapidezDesplazamiento = 10.0f;
    public Camera camaraPrimeraPersona;
    public GameObject proyectil;

    void Start()
    {
        //Salto.
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>(); cont = 0;
        GestorDeAudio.instancia.ReproducirSonido("Musica");
        Cursor.lockState = CursorLockMode.Locked;
        //UI.
        textoGanaste.text = "";
        setearTextos();

    }
    private void setearTextos()
    {
        textoCantidadRecolectados.text = "Lingotes recolectados: " + cont.ToString(); if (cont >=20)
        {
            textoGanaste.text = "Objetivo Completado!";
        }
    }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;
        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;
        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;
            //Disparo por proyectiles.
            GameObject pro;
            pro = Instantiate(proyectil, ray.origin, transform.rotation);
            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * 15, ForceMode.Impulse);
            Destroy(pro, 5);
            //Invocar script del objeto que devuelve el Raycasting.
            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
            {
                Debug.Log("El rayo tocó al objeto: " + hit.collider.name);
                if (hit.collider.name.Substring(0, 3) == "Bot")
                {
                    GameObject objetoTocado = GameObject.Find(hit.transform.name);
                    ControlBot scriptObjetoTocado = (ControlBot)objetoTocado.GetComponent(typeof(ControlBot));
                    if (scriptObjetoTocado != null)
                    {
                        scriptObjetoTocado.recibirDaño();
                    }
                }
            }
        }
        //Para la mecanica de salto.
        if (Input.GetKeyDown(KeyCode.Space) && EstaEnPiso())
        {
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
        }
    }
    private bool EstaEnPiso()
    {
        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coleccionable") == true)
        {
            other.gameObject.SetActive(false);
        }
    }
}