﻿using UnityEngine;

public class Configuracion : MonoBehaviour
{
    private void Start()
    {
        if(string.IsNullOrEmpty(PlayerPrefs.GetString("Resolution")))
        {
            PlayerPrefs.SetString("Resolucion", "HD");
        }
        Debug.Log("La resolucion es: " + PlayerPrefs.GetString("Resolucion"));
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            PlayerPrefs.SetString("Resolucion", "HD");
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            PlayerPrefs.SetString("Resolucion", "FullHD");
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            PlayerPrefs.SetString("Resolucion", "4K");
        }
    }
}
